Package: CleanGeoStreamR
Type: Package
Title: Automatic Curation of Spatially Annotated Data
Version: 1.0.0
Author: Ilhan Mutlu [aut, cre], Jana Schor [aut, cre]
Maintainer: Ilhan Mutlu <ilhan.mutlu@ufz.de>
Description: This package provides tools for the automatic curation of spatially annotated data. It facilitates the processing, cleaning, and organizing of geographic and spatial data to ensure accuracy and usability for various spatial analysis tasks.
License: GPL-3
Encoding: UTF-8
LazyData: true
Depends:
    R (>= 3.5.0)
Imports:
    grDevices,
    DT,
    tidyverse,
    optparse,
    lubridate,
    tidygeocoder,
    parallel,
    pheatmap,
    R.utils,
    rnaturalearth,
    sf,
    maptools,
    sp,
    rworldmap,
    leaflet,
    jsonlite,
    magrittr,
    geosphere,
    stringi,
    dplyr,
    ggplot2,
    stringr,
    tibble,
    tidyr
Suggests:
    testthat,
    knitr,
    rmarkdown
VignetteBuilder: knitr
URL: https://codebase.helmholtz.cloud/department-computational-biology/software/cleangeostreamr
BugReports: https://codebase.helmholtz.cloud/department-computational-biology/software/cleangeostreamr/-/issues
RoxygenNote: 7.3.1
