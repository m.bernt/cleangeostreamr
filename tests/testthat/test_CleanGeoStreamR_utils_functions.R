library("tidyverse")
library("testthat")

source("R/CleanGeoStreamR_utils_functions.R") # This is only needed if your project is not a package



testthat::test_that(desc = "Check if filepath correction works", {
  testthat::expect_equal(file.path2("data/", "results", "file.txt"),
                         expected = "data/results/file.txt")
})

testthat::test_that(desc = "Provide correct input types", {
  testthat::expect_error(draw_cheos_numbers(data = 5, regexp = "Variable df is not of type tibble."))
  testthat::expect_error(draw_cheos_numbers(data = tibble("a" = "aa", "b" = "ba", "n" = 42),
                                            col_filter = 3, selected_filter = 7,
                                            col_entity = TRUE, selected_entities = 23,
                                            col_N = 3),
                         regexp = "ERROR: The following arguments are not characters:  3, 7, TRUE, 23, 3")
})

testthat::test_that(desc = "Provide correct column names", {
  testthat::expect_error(draw_cheos_numbers(data = tibble("a" = "aa", "b" = "ba", "n" = 42),
                                            col_filter = "aa", col_entity = "bb", col_N = "N"),
                         regexp = "ERROR: The following columns are not present in data:  aa, bb, N")
})

testthat::test_that(desc = "Check if all elements in list are characters", {
  testthat::expect_equal(check_all_characters(args = list("a", "b", "cdefg")), expected = TRUE)
  testthat::expect_error(check_all_characters(args = list("a", "b", 5)),
                         regexp = "ERROR: The following arguments are not characters:  5")
})

testthat::test_that(desc = "Check if all column names are present in tibble", {
  testthat::expect_equal(check_all_contained(data = tibble("a" = "aa", "b" = "ba", "n" = 42),
                                             cols = c("a", "b")), expected = TRUE)
  testthat::expect_error(check_all_contained(data = tibble("a" = "aa", "b" = "ba", "n" = 42),
                                             cols = c("a", "b", "c", "d")),
                         regexp = "ERROR: The following columns are not present in data:  c, d")
})


test_that("check_all_characters function works correctly", {

  # Test case 1: Test with all character arguments
  args_1 <- list("arg1", "arg2", "arg3")
  expect_true(check_all_characters(args_1))

  # Test case 2: Test with a mix of character and non-character arguments
  args_2 <- list("arg1", 123, "arg3")
  expect_error(check_all_characters(args_2),
               regexp = "ERROR: The following arguments are not characters:  123")

  # Test case 3: Test with empty arguments
  args_3 <- list()
  expect_true(check_all_characters(args_3))

  # Test case 4: Test with single character argument
  args_4 <- list("arg1")
  expect_true(check_all_characters(args_4))

  # Test case 5: Test with single non-character argument
  args_5 <- list(123)
  expect_error(check_all_characters(args_5),
               regexp = "ERROR: The following arguments are not characters:  123")

})

test_that("check_all_contained function works correctly", {

  # Mock data frame for testing
  mock_data <- data.frame(col1 = c(1, 2, 3),
                           col2 = c("a", "b", "c"),
                           stringsAsFactors = FALSE)

  # Test case 1: Test with all columns present
  cols_1 <- c("col1", "col2")
  expect_true(check_all_contained(mock_data, cols_1))

  # Test case 2: Test with a mix of present and absent columns
  cols_2 <- c("col1", "col3")
  expect_error(check_all_contained(mock_data, cols_2),
               regexp = "ERROR: The following columns are not present in data:  col3")

  # Test case 3: Test with empty column names
  cols_3 <- character(0)
  expect_true(check_all_contained(mock_data, cols_3))

  # Test case 4: Test with single column present
  cols_4 <- "col1"
  expect_true(check_all_contained(mock_data, cols_4))

  # Test case 5: Test with single column absent
  cols_5 <- "col3"
  expect_error(check_all_contained(mock_data, cols_5),
               regexp = "ERROR: The following columns are not present in data:  col3")

})

test_that("file.path2 function works correctly", {

  # Test case 1: Test with normal file path generation
  expected_path_1 <- file.path("dir1", "dir2", "file.txt")
  actual_path_1 <- file.path2("dir1", "dir2", "file.txt")
  expect_equal(actual_path_1, expected_path_1)

 # # Test case 2: Test with duplicate slashes
 # expected_path_2 <- file.path("dir1", "dir2", "file.txt")
 # actual_path_2 <- file.path2("dir1//", "/dir2", "//file.txt")
 # expect_equal(actual_path_2, expected_path_2)

  # Test case 3: Test with different file separators
  if (.Platform$file.sep == "\\") {
    expected_path_3 <- "dir1\\dir2\\file.txt"
  } else {
    expected_path_3 <- "dir1/dir2/file.txt"
  }
  actual_path_3 <- file.path2("dir1", "dir2", "file.txt", fsep = .Platform$file.sep)
  expect_equal(actual_path_3, expected_path_3)


})

test_that("to_camel_case function works correctly", {

  # Test case 1: Test with single string
  input_1 <- "hello_world"
  expected_1 <- "HelloWorld"
  expect_equal(to_camel_case(input_1), expected_1)

  # Test case 2: Test with multiple strings
  input_2 <- c("hello_world", "some_text_here")
  expected_2 <- c("HelloWorld", "SomeTextHere")
  expect_equal(to_camel_case(input_2), expected_2)

  # Test case 3: Test with strings containing special characters
  input_3 <- c("hello-world", "some_text_here")
  expected_3 <- c("HelloWorld", "SomeTextHere")
  expect_equal(to_camel_case(input_3), expected_3)

  # Test case 4: Test with empty string
  input_4 <- ""
  expected_4 <- ""
  expect_equal(to_camel_case(input_4), expected_4)

})

test_that("create_dt function works correctly", {

  # Test case 1: Test with data and caption
  input_1 <- mtcars
  caption_1 <- "This is a DataTable caption."
  expected_1 <- datatable(input_1,
                          caption = caption_1,
                          extensions = 'Buttons',
                          options = list(dom = 'Blfrtip',
                                         buttons = c('copy', 'csv'),
                                         lengthMenu = list(c(10, 100, -1),
                                                           c(10, 100, 1000, "All"))))
  expect_equal(create_dt(input_1, caption_1), expected_1)

  # Test case 2: Test with data and no caption
  input_2 <- mtcars
  expected_2 <- datatable(input_2,
                          extensions = 'Buttons',
                          options = list(dom = 'Blfrtip',
                                         buttons = c('copy', 'csv'),
                                         lengthMenu = list(c(10, 100, -1),
                                                           c(10, 100, 1000, "All"))))
  expect_equal(create_dt(input_2), expected_2)

  # Test case 3: Test with empty data
  input_3 <- NULL
  expected_3 <- datatable(NULL,
                          extensions = 'Buttons',
                          options = list(dom = 'Blfrtip',
                                         buttons = c('copy', 'csv'),
                                         lengthMenu = list(c(10, 100, -1),
                                                           c(10, 100, 1000, "All"))))
  expect_equal(create_dt(input_3), expected_3)

})

test_that("setup_data_directories function works correctly", {

  # Test case 1: Test with existing basepath and no creation
  #basepath_1 <- tempdir()
  #expected_1 <- list(
  #  input = file.path(basepath_1, "input"),
  #  output = file.path(basepath_1, "output"),
  #  rdata = file.path(basepath_1, "rdata")
  #)
  #expect_equal(setup_data_directories(basepath = basepath_1), expected_1)

  # Test case 2: Test with non-existing basepath and no creation
  basepath_2 <- file.path(tempdir(), "nonExistingPath")
  expect_error(setup_data_directories(basepath = basepath_2),
               "ERROR: The basepath does not exist")

  # Test case 3: Test with non-existing basepath and creation allowed
  basepath_3 <- file.path(tempdir(), "non_existing_path")
  expected_3 <- list(
    input = file.path(basepath_3, "input"),
    output = file.path(basepath_3, "output"),
    rdata = file.path(basepath_3, "rdata")
  )
  setup_dirs_3 <- setup_data_directories(basepath = basepath_3, create_if_not_exists = TRUE)
  expect_equal(setup_dirs_3, expected_3)

  # Test case 4: Test with prefix and non-existing basepath and creation allowed
  prefix_4 <- "~"
  basepath_4 <- "non_existing_path_with_prefix"
  expected_4 <- list(
    input = file.path(normalizePath(file.path(prefix_4, basepath_4)), "input"),
    output = file.path(normalizePath(file.path(prefix_4, basepath_4)), "output"),
    rdata = file.path(normalizePath(file.path(prefix_4, basepath_4)), "rdata")
  )
  setup_dirs_4 <- setup_data_directories(prefix = prefix_4, basepath = basepath_4, create_if_not_exists = TRUE)
  expect_equal(setup_dirs_4, expected_4)

})


test_that("intersect_groups returns correct intersection for simple case", {
  data <- tibble(
    group = c('A', 'A', 'B', 'B', 'C', 'C'),
    value = c(1, 2, 2, 3, 2, 4)
  )
  result <- intersect_groups(data, "group", "value")
  expected <- c(2)
  expect_equal(result, expected)
})

test_that("intersect_groups returns empty for no common values", {
  data <- tibble(
    group = c('A', 'A', 'B', 'B', 'C', 'C'),
    value = c(1, 2, 3, 4, 5, 6)
  )
  result <- intersect_groups(data, "group", "value")
  expected <- numeric(0)
  expect_equal(result, expected)
})


test_that("intersect_groups handles missing values", {
  data <- tibble(
    group = c('A', 'A', 'B', 'B', 'C', 'C'),
    value = c(1, 2, NA, 2, 2, NA)
  )
  result <- intersect_groups(data, "group", "value")
  expected <- c(2)
  expect_equal(result, expected)
})

test_that("intersect_groups works with character values", {
  data <- tibble(
    group = c('A', 'A', 'B', 'B', 'C', 'C'),
    value = c('apple', 'banana', 'banana', 'cherry', 'banana', 'date')
  )
  result <- intersect_groups(data, "group", "value")
  expected <- c('banana')
  expect_equal(result, expected)
})

test_that("intersect_groups works with different data types", {
  data <- tibble(
    group = c('A', 'A', 'B', 'B', 'C', 'C'),
    value = c(1, 'a', 1, 'a', 1, 'a')
  )
  result <- intersect_groups(data, "group", "value")
  expected <- c(1, 'a')
  expect_equal(result, expected)
})

# Create temporary files for testing
test_that("loadRData function works correctly", {
  # Create a temporary file
  temp_file <- tempfile(fileext = ".RData")

  # Save an object to the temporary file
  test_object <- list(a = 1, b = 2)
  save(test_object, file = temp_file)

  # Test if the function correctly loads the data
  loaded_object <- loadRData(temp_file)
  expect_equal(loaded_object, test_object)

  # Clean up the temporary file
  unlink(temp_file)
})

test_that("loadRData function handles different types of data", {
  # Create and test with different types of data
  temp_file <- tempfile(fileext = ".RData")

  # Integer vector
  test_vector <- 1:10
  save(test_vector, file = temp_file)
  loaded_vector <- loadRData(temp_file)
  expect_equal(loaded_vector, test_vector)

  # Clean up the temporary file
  unlink(temp_file)

  # Data frame
  temp_file <- tempfile(fileext = ".RData")
  test_df <- data.frame(x = 1:5, y = letters[1:5])
  save(test_df, file = temp_file)
  loaded_df <- loadRData(temp_file)
  expect_equal(loaded_df, test_df)

  # Clean up the temporary file
  unlink(temp_file)

  # Matrix
  temp_file <- tempfile(fileext = ".RData")
  test_matrix <- matrix(1:9, nrow = 3)
  save(test_matrix, file = temp_file)
  loaded_matrix <- loadRData(temp_file)
  expect_equal(loaded_matrix, test_matrix)

  # Clean up the temporary file
  unlink(temp_file)
})


test_that("loadRData function handles file with no objects", {
  # Create a temporary file
  temp_file <- tempfile(fileext = ".RData")

  # Save no objects to the temporary file
  save(list = character(0), file = temp_file)

  # Test if the function returns an error
  expect_error(loadRData(temp_file))

  # Clean up the temporary file
  unlink(temp_file)
})