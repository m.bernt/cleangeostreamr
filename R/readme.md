# R Directory Overview

The `R` directory contains the core scripts that implement the main functionality of the package. Below is an overview of the scripts and their roles:

## Main Scripts

1. **read_curate_chemical_data.R**
   - This script provides the main functions for reading and curating chemical data.
   - It includes processes for parsing raw data, cleaning, and ensuring data integrity.

2. **read_curate_spatial_data.R**
   - This script contains the primary functions for reading and curating spatial data.
   - It includes procedures for transforming raw spatial data, conducting spatial operations, and verifying data accuracy.

## Supporting Scripts

1. **CleanGeoStreamR_utils_functions.R**
   - Contains utility functions that support various operations within the package.
   - These functions are used across different modules for common tasks such as data validation and format conversion.

2. **read_curate_chemical_data_functions.R**
   - Includes functions that are specifically used by `read_curate_chemical_data.R`.
   - These functions handle detailed tasks related to chemical data processing, ensuring the main script remains clean and focused.

3. **read_curate_spatial_data_functions.R**
   - Contains functions primarily used by `read_curate_spatial_data.R`.
   - These functions perform specific operations related to spatial data handling, supporting the main spatial data script.
