# Example Input - Miscellaneous Files

This README file provides detailed information regarding the contents of the files included in this directory. Included JSON files are used for determining country codes and neighboring countries.

## Included Files

### 1. Country Codes File: `countries.json`

This file contains a mapping of country names to their respective two-character ISO codes.

### 2. Neighboring Countries File: `neighbors.json`

This file includes data on neighboring countries for each country, used to determine geographical relationships between countries.

These files are used within the package to handle country code identification and to understand geographical proximities.
