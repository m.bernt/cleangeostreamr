# Input Directory

The `input` directory serves as the default input directory for the `CleanGeoStreamR` package. Users can customize this directory according to their specific data sources and requirements. Below is an overview of the subdirectories contained within:

## NORMAN

The `NORMAN` directory includes data from the NORMAN network, focusing on emerging pollutants and related research. This directory contains important datasets regarding the proper execution of the CleanGeoStreamR package.
In this example directory a reduced version of the surface waters and substance data from the Norman database is included. Original files can be found at: [Zenodo Dataset](https://zenodo.org/records/11395194)

## CompTox

This directory contains data related to computational toxicology. For detailed information about the data sources and contents, please refer to the `CompTox` directory.

## MANUAL_CURATION

The `MANUAL_CURATION` directory is designated for manually curated data. 
This folder contain manually curated data for river basin names and water body names.

## MISC

The `MISC` directory is meant for miscellaneous data files or datasets that do not fit into the other predefined categories. For details about the contents of this directory, please refer to the `MISC` directory.

## NaturalEarth

This directory contains geographical data from the Natural Earth dataset. Detailed information about the contents and structure of this directory can be found within the `NaturalEarth` directory.

### Customizing the Input Directory

Users have the flexibility to customize the input directory according to their needs. They can add, remove, or rename subdirectories and files as required. 

**Note**: When changing the input directory, ensure that the package functions are appropriately updated to reflect the new directory structure.

## Additional Information

For more detailed documentation on specific datasets or data sources within each subdirectory, please refer to the corresponding directory's documentation.

---
