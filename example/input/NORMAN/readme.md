# Example Input - NORMAN Files

This README file provides detailed information regarding the contents of the files included in this directory. These files are essential for the proper functioning of the R package and serve as examples of the required input data.

## Included Files

### 1. Empodat File: `Surface_water_2024-04-24-154511_REDUCED.csv`

This file can be abbreviated as the **empodat file**. It is one of the most important input files for the package as it contains the chemical occurrence data of European fresh water bodies. Only a reduced version of the original file is provided directly with the package. Users can also obtain such a dataset directly from the [NORMAN EMPODAT database](https://www.norman-network.com/nds/empodat/chemicalSearch.php).

#### Description:
- **EMPODAT**: A database of geo-referenced monitoring and bio-monitoring data on emerging substances in various matrices including water, sediments, biota, SPM, soil, sewage sludge, and air.
- **Contents**: Chemical occurrence data for fresh water bodies in Europe.

### 2. Header File: `Surface_water_2024-04-24-154511_header.csv`

This file can be abbreviated as the **header file**. The structure of headers in the empodat file is unique, typically spanning three rows. As a result, such a header file is necessary to apply data processing algorithms properly.

#### Description:
- **Original Headers**: The first 3 rows.
- **Merged Headers**: The 4th row, containing new names to be used in the related sections of the provided scripts.
- **Column Types**: The 5th row, indicating the type of the 4th row headers (character `c` or numeric `d`).

More details regarding how to prepare the header file can be found in the [How_to_Prepare_Header_File.md](How_to_Prepare_Header_File.md) document.

### 3. Susdat File: `susdat_2024-04-24-080823_REDUCED.csv`

This file can be abbreviated as **susdat file**. It includes important identifiers and definitions regarding the substances. The original data source is the NORMAN substance database, which can be accessed [here](https://www.norman-network.com/nds/susdat/).

#### Description:
- **Included information covers (but not limited to)**:
  - `Norman_SusDat_ID`
  - `Name`
  - `Name_Dashboard`
  - `Name_ChemSpider`
  - `Name_IUPAC`
  - `Synonyms_ChemSpider`
  - `CAS_RN`
  - `CAS_RN_Dashboard`
  - `CAS_RN_PubChem`
  - `CAS_RN_Cactus`
  - `CAS_RN_ChemSpider`
  - `Reliability_of_CAS_ChemSpider`
  - `SMILES`
  - `SMILES_Dashboard`
  - `StdInChI`
  - `StdInChIKey`
  - `MS_Ready_SMILES`
  - `MS_Ready_StdInChI`
  - `MS_Ready_StdInChIKey`
  - `PubChem_CID`
  - `ChemSpiderID`
  - `DTXSID`
  - `Molecular_Formula`
