# inst Directory

This directory is reserved for files that should be included with the installed package.
Currently, we provide the `example` directory in the main file structure of the repository for the users.

## Example Directory

The `example` directory, which is located alongside the `inst` directory, provides extensive documentation and resources for users.
Detailed information is provided directly inside the `example` directory. 
